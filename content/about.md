+++
title = "About"
date = "2018-10-12"
sidemenu = "true"
description = "About me and this site"
+++

It is said that lazy engineers are the best engineers!

I try to apply lazyness in my day to day job :)

Thanks for reading!
