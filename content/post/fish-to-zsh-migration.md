---
title: "Fish to Zsh Migration"
date: 2018-10-12T11:30:47+02:00
---

Ok that's it, I cannot handle the stupidity of fish not being compatible with posix sh anymore!

I mean, I can understand a bit of incompatibility but not being able to use $() or ! or {} in my commands without fish starting to get crazy is too much.

A colleague suggested that I take a look at zsh and it's pretty much doing what I want, and to be honest I was surprised at how fast I made the switch.

FYI the plugins which made zsh work for me are oh-my-zsh plugins, thus the reason why I went for that instead of vanilla zsh. I might try to have a cleaner setup in the future but for now that'll have to do.

Let's review what I did:

# Install oh-my-zsh

I know I should not trust random scripts on the internet so I had a look at the oh-my-zsh install script before I ran the command (lazyness does not imply no security!).

Then it's just a matter of pasting that into a terminal:

```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

Everything installed fine for me and I ended-up directly in an ugly looking zsh!

# Configure missing plugins

The history search and command completion behaviour in zsh is not the same as in fish but people went through the hassle of implementing that for us!

Those are the plugins that I ended-up installing:

* https://github.com/zsh-users/zsh-autosuggestions
* https://github.com/zsh-users/zsh-history-substring-search
* https://github.com/zsh-users/zsh-syntax-highlighting.git

We need to clone those in the oh-my-zsh plugins folder:

```
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-history-substring-search $ZSH_CUSTOM/plugins/zsh-history-substring-search
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
```

And then adapt the .zshrc to use them:

```
plugins=(
  git
  zsh-autosuggestions
  zsh-history-substring-search
  zsh-syntax-highlighting
)
```

# Configure prompt

I was lucky on that one: the prompt that I use for fish (https://github.com/simnalamburt/shellder) has a zsh version!

The author suggests to use zplug and that's what I did since I have no idea how to install that theme in oh-my-zsh:

```
brew install zplug
```

Then it's just a matter of adding the plugin into my zshrc (+ a few stuff which the brew command told me):

```
export ZPLUG_HOME=/usr/local/opt/zplug
source $ZPLUG_HOME/init.zsh

zplug 'simnalamburt/shellder', as:theme

if ! zplug check; then
    zplug install
fi
zplug load
```

The zplug check call is not *necessary* but it makes the startup of zsh a tad faster and less verbose.

